var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    id :  {type: String, required: true, unique: true},
    name: {type: String, required: true},
    username: { type: String, required: true, unique: true, lowercase: true },
    profile_pic: {type: String, required: true},
    created_at: Date,
    updated_at: Date
});

userSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

    next();
});

var User = mongoose.model('User', userSchema);

module.exports = User;